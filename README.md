# GeeTsee
This is GeeTsee, an android game that was inspired by the children game here in Ethiopia in which each player takes turns in writing the nouns that can be constructed by the stated letter for all the categories. If one doesn't know the noun constructed from that letter for any of the categories, one can skip and leave it blank.

The score is evaluated by assigning each category a max point of 10 and every player gets a 10 out of 10 if one has answered correctly and there are no duplicates. If there are duplicates, half a point is given to the duplicate answers. One game session ends when one player halts the game after answering correctly for all the categories.

I am hoping to develop this game for two reasons:
1. to improve as a android developer.
2. to use my spare time for productive use.

I will develop this game in my spare time so it won't be a quick dev't project.

I will be using Android Studio for the IDE and java for the programming language. Any frameworks, libraries used will be describe here (at the moment I am not using any).

> N.B. As with everything attached to this dev't project, this read me file is subject to changes.